<?php

require __DIR__ . '/vendor/autoload.php';

use Transip\Api\Library\TransipAPI;
use Transip\Api\Library\Entity\Domain\DnsEntry as DnsEntry;

use Badcow\DNS\Zone;
use Badcow\DNS\Rdata\Factory;
use Badcow\DNS\ResourceRecord;
use Badcow\DNS\AlignedBuilder;

class BackupDNS {

    private $loginName;
    private $privateKey;
    private $api;

    public function __construct()
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $dotenv->load();

        if (empty($_ENV['TRANSIP_USERNAME']))
        {
            throw new \RuntimeException('TransIP username not found!');
        }

        if (!file_exists($_ENV['TRANSIP_PRIVATEKEY']))
        {
            throw new \RuntimeException('TransIP Private Key file not found!');
        }

        $this->loginName = $_ENV['TRANSIP_USERNAME'];
        $this->privateKey = file_get_contents($_ENV['TRANSIP_PRIVATEKEY']);

    }

    public function login()
    {
        $this->api = new TransipAPI(
            $this->loginName,
            $this->privateKey,
            true
        );
    }

    public function createTempDirectory()
    {
        // Create temp directory
        @mkdir("./tmp", 0700);
    }

    public function deleteTempDirectory()
    {
        $it = new RecursiveDirectoryIterator("./tmp", RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it,
            RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir("./tmp");
    }

    public function getDomains()
    {
        $allDomains = $this->api->domains()->getAll();


        foreach ($allDomains as $domain) {
            $dnsentries = $this->api->domainDns()->getByDomainName($domain->getName());
            $this->exportDNSZone($dnsentries, $domain->getName());

        }
    }

    public function exportDNSZone($dnsentries, $domain)
    {


        $zone = new Zone($domain . '.');
        $zone->setDefaultTtl(1); // 1 means automatic

        foreach ($dnsentries as $dnsEntry) {

            $rr = new ResourceRecord;
            $rr->setName($dnsEntry->getName());
            switch ($dnsEntry->getType()) {
                case DnsEntry::TYPE_A:
                    $rr->setRdata(Factory::A($dnsEntry->getContent()));
                    break;
                case DnsEntry::TYPE_AAAA:
                    $rr->setRdata(Factory::Aaaa($dnsEntry->getContent()));
                    break;
                case DnsEntry::TYPE_CNAME:
                    $rr->setRdata(Factory::Cname($dnsEntry->getContent()));
                    break;
                case DnsEntry::TYPE_MX:
                    list($pref, $cont) = explode(' ', $dnsEntry->getContent());
                    $rr->setRdata(Factory::Mx($pref, $cont));
                    break;
                case DnsEntry::TYPE_TXT:
                    $rr->setRdata(Factory::Txt($dnsEntry->getContent()));
                    break;
                case DnsEntry::TYPE_SRV:
                    $srvRecord = explode($dnsEntry->getContent(), ' ');
                    if (count($srvRecord) > 1) {
                        $rr->setRdata(Factory::SRV($srvRecord[0], $srvRecord[1], $srvRecord[2], $srvRecord[3]));
                    }
                    break;
                case DnsEntry::TYPE_NS:
                    $rr->setRdata(Factory::NS($dnsEntry->getContent()));
                    break;
                default:
                    throw new \RuntimeException('Migrating ' . $dnsEntry->getType() . ' records is not implemented');
            }
            $zone->addResourceRecord($rr);
        }
        // Build DNS Zone file
        $builder = new AlignedBuilder();
        // generate unique domainname filename
        $fileName = sprintf('./tmp/%s.dns', $domain);
        // Write zone file
        file_put_contents($fileName, $builder->build($zone));
        echo "Created zone file for " . $domain . PHP_EOL;
    }

    public function createZipFile()
    {
        $fileName = sprintf("./dnsbackup-%s.zip", date('Y-m-d'));
        $zip = new ZipArchive;
        $res = $zip->open($fileName, ZipArchive::CREATE);
        if ($res === TRUE) {
            $files = scandir("./tmp");
            foreach($files as $file)
            {
                if ($file == '.' || $file == '..') continue;

                $zip->addFile("./tmp/" . $file);
            }
            if ($zip->close() === false)
            {
                throw new \RuntimeException('Error when creating zip file.');
            }
            echo "zip $fileName created" . PHP_EOL;
        } else {
            echo 'failed' . PHP_EOL;
        }
    }

    public function backup()
    {
        $this->login();
        $this->createTempDirectory();
        $this->getDomains();
        $this->createZipFile();
        $this->deleteTempDirectory();
    }

}

$backup = new BackupDNS();
$backup->backup();
